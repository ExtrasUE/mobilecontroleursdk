import React, { Component } from 'react';
import { Text, View, StyleSheet, Dimensions } from 'react-native';
import { Constants, Accelerometer, Gyroscope } from 'expo-sensors';

export default class App extends Component {
  state = {
    accelerometerData: { x: 0, y: 0, z: 0 },
    accelerometerVector: 0,
    gyroscopeData: { x: 0, y: 0, z: 0 },
    gyroscopeVector:0
  };
  compteur = 0;

  componentWillUnmount() {
    this._unsubscribeFromAccelerometer();
    this._unsubscribeFromGyroscope();
  }

  componentDidMount() {
    this._subscribeToAccelerometer();
    this._subscribeToGyroscope();
  }

  componentWillMount() {
    const { width, height } = Dimensions.get('window');
    this.screenWidth = width;
    this.screenHeight = height;
    this.boxWidth = this.screenWidth / 10.0;
  }
  shouldComponentUpdate(nextProps, nextState){
    if(((this.state.gyroscopeVector > 1 && nextState.gyroscopeVector < 1) || this.state.gyroscopeVector < 1 && nextState.gyroscopeVector > 1) && nextState.gyroscopeVector - this.state.gyroscopeVector > 1){
      console.log("STEP GYR");
      this.compteur++;
      console.log(this.compteur);
    }
    return true;
  }

  _subscribeToAccelerometer = () => {
    this._accelerometerSubscription = Accelerometer.addListener(accelerometerData => {
        this.setState({ accelerometerData })
        this.setState({accelerometerVector:Math.sqrt(accelerometerData.x*accelerometerData.x+accelerometerData.y*accelerometerData.y+accelerometerData.z*accelerometerData.z)});
    }
    );
  };

  _subscribeToGyroscope = () => {
    this._gyroscopeSubscription = Gyroscope.addListener(gyroscopeData =>{ 
      this.setState({ gyroscopeData });
      this.setState({gyroscopeVector:Math.sqrt(gyroscopeData.x*gyroscopeData.x+gyroscopeData.y*gyroscopeData.y+gyroscopeData.z*gyroscopeData.z)});
    }
    );
  };

  _unsubscribeFromAccelerometer = () => {
    this._accelerometerSubscription && this._acceleroMeterSubscription.remove();
    this._accelerometerSubscription = null;
  };

  _unsubscribeFromGyroscope = () => {
    this._gyroscopeSubscription && this._gyroscopeSubscription.remove();
    this._gyroscopeSubscription = null;
  };


  render() {
    return (
      <View style={styles.container}>
        <View style={styles.textContainer}>
          <Text style={styles.paragraph}>Accelerometer :</Text>
          <Text style={styles.paragraph}>
            x = {this.state.accelerometerData.x.toFixed(2)}
            {', '}y = {this.state.accelerometerData.y.toFixed(2)}
            {', '}z = {this.state.accelerometerData.z.toFixed(2)}
          </Text>
          <Text style={styles.paragraph}>
            vector : {this.state.accelerometerVector}
          </Text>
          <Text style={styles.paragraph}>Gyroscope :</Text>
          <Text style={styles.paragraph}>
            x = {this.state.gyroscopeData.x.toFixed(2)}
            {', '}y = {this.state.gyroscopeData.y.toFixed(2)}
            {', '}z = {this.state.gyroscopeData.z.toFixed(2)}
          </Text>
          <Text style={styles.paragraph}>
            vector : {this.state.gyroscopeVector}
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 10,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
  textContainer: {
    position: 'absolute',
    top: 40,
  },
});
